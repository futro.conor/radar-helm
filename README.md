# RADAR Helm
## Description & Rationale
This helm chart deploys the OpenEthereum container built from that repository. As a stateful application it's deployed in a StatefulSet as opposed to a Deployment. Storage is also provisioned for storing the blockchain and a service is created for access to RPC and to allow Prometheus to scrape metrics. Most values are paramaterized to allow for easy adjustment depending on the particulars of the deployment.

## CI/CD
This is a very simple pipeline, it templates the helm chart to populate all of the values and then applies it with kubectl so that the lifecycle is not managed by helm. Additionally it is triggered whenever the OpenEthereum job is built to ensure the container is the current version.
